import React from 'react'
import { Link } from 'react-router'

export default class Navigation extends React.Component {
  render() {
    return (
      <aside className='main-sidebar'>
        <section className='sidebar'>
          <div className='user-panel'>
            <div className='info'>
              <p>Admin</p>
              <a href='#'><i className='fa fa-circle text-success'></i> Online</a>
            </div>
          </div>

          <ul className='sidebar-menu' data-widget='tree'>
            <li className='header'>MAIN NAVIGATION</li>
            <li className='active treeview'>
              <a href='#'>
                <i className='fa fa-dashboard'></i> <span>Sample 1</span>
                <span className='pull-right-container'>
                  <i className='fa fa-angle-left pull-right'></i>
                </span>
              </a>
              <ul className='treeview-menu'>
                <li className='active'><a href='#'><i className='fa fa-circle-o'></i> Sample 1-1</a></li>
                <li><a href='#'><i className='fa fa-circle-o'></i> Sample 1-2</a></li>
              </ul>
            </li>
            <li>
              <a href='#'>
                <i className='fa fa-envelope'></i> <span>Sample 2</span>
              </a>
            </li>
            <li className='treeview'>
              <a href='#'>
                <i className='fa fa-folder'></i> <span>Sample 3</span>
                <span className='pull-right-container'>
                  <i className='fa fa-angle-left pull-right'></i>
                </span>
              </a>
              <ul className='treeview-menu'>
                <li><a href='#'><i className='fa fa-circle-o'></i> Invoice</a></li>
                <li><a href='#'><i className='fa fa-circle-o'></i> Profile</a></li>
                <li><a href='#'><i className='fa fa-circle-o'></i> Login</a></li>
                <li><a href='#'><i className='fa fa-circle-o'></i> Register</a></li>
                <li><a href='#'><i className='fa fa-circle-o'></i> Lockscreen</a></li>
                <li><a href='#'><i className='fa fa-circle-o'></i> 404 Error</a></li>
                <li><a href='#'><i className='fa fa-circle-o'></i> 500 Error</a></li>
                <li><a href='#'><i className='fa fa-circle-o'></i> Blank Page</a></li>
                <li><a href='#'><i className='fa fa-circle-o'></i> Pace Page</a></li>
              </ul>
            </li>
          </ul>
        </section>

      </aside>
    )
  }
}