import React from 'react';
import { render } from 'react-dom';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import Root from './containers/Root';
import configureStore from './store/configureStore';

import { AppContainer } from 'react-hot-loader';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);
const rootEl = document.getElementById('root');

const render_app = Component =>
  render(
    <AppContainer>
      <Root store={store} history={history} />
    </AppContainer>,
    rootEl
  );

render_app(Root);

if (module.hot) {
  module.hot.accept('./containers/Root', () => render_app(Root));
}