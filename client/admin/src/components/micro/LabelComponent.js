import React from 'react'

export default class LabelComponent extends React.Component {
  render() {
    let { text, theme } = this.props
    return (
      <span className={theme}>{text}</span>
    )
  }
}