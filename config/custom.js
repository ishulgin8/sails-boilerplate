module.exports.custom = {
  http_codes: {
    success: 201,
    server_error: 500,
    not_found: 404,
    not_allowed: 405
  },
  get_custom_template: () => ({
    status: false,
    msg: "",
    http_code: 400,
    data: null
  }),
  get_success_template: () => ({
    status: true,
    msg: "",
    http_code: 201,
    data: null
  }),
  get_error_template: () => ({
    status: false,
    msg: "",
    http_code: 400,
    data: null
  })
}
