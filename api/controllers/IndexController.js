/**
 * IndexController
 *
 * @description :: Server-side logic for managing Indices
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index: (req, res) => {
		return res.view('pages/index', {
			action: 'index',
			user: req.user ? req.user : {}
		})
	}
};
