export const defaults = {
  production: {
    api_endpoint: 'http://example.com/api/',
    server_root: 'http://example.com/',
  },
  development: {
    api_endpoint: 'http://127.0.0.1:1801/api/',
    server_root: 'http://127.0.0.1:1801/'
  },
  none_category: [
    {
      id: '',
      title: 'None',
      desc: ''
    }
  ],
  product_types: [
    {
      id: 0,
      label: 'Regular'
    },
    {
      id: 2,
      label: 'Promotion'
    }
  ]
}