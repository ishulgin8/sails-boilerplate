/**
 * isGuest
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any guest user
 * `
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */

module.exports = (req, res, next) => {
  if (typeof (req.user) === 'undefined') {
    return next()
  }
  return res.redirect('/')
}
