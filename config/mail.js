module.exports.mail = {
  transport: {
    host: 'smtp.sendgrid.net',
    port: 587,
    secure: false,
    auth: {
      user: '',
      pass: ''
    }
  },
  options: {
    from: '',
    from_name: ''
  },
  sg_key: ''
}