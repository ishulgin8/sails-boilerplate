import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';

import DashboardComponent from './components/DashboardComponent';

export default <Route path='/account' component={App}>
  <IndexRoute component={DashboardComponent} />
</Route>
