const gulp = require('gulp');
const gulpif = require('gulp-if');
const uglifycss = require('gulp-uglifycss');
const sass = require('gulp-sass');
const coffee = require('gulp-coffee');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const gzip = require('gulp-gzip');

gulp.task('sass', () => {
    return gulp.src([
        'assets/sass/**/*.scss'])
        .pipe(gulpif(/[.]scss/, sass()))
        .pipe(uglifycss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/css/'))
});

gulp.task('watch_sass', () => {
  return gulp.watch('assets/sass/**/*.scss', function () {
    gulp.src([
        'assets/sass/**/*.scss'])
        .pipe(gulpif(/[.]scss/, sass()))
        .pipe(uglifycss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/css/'))
  })
});

gulp.task('coffee', () => {
    return gulp.src([
        'assets/coffee/**/*.coffee'])
        .pipe(gulpif(/[.]coffee/, coffee({bare: true})))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('watch_coffee', () => {
  return gulp.watch('assets/coffee/**/*.coffee', function () {
    gulp.src([
        'assets/coffee/**/*.coffee'])
        .pipe(gulpif(/[.]coffee/, coffee({bare: true})))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/js/'));
  })
});

gulp.task('css_vendor', () => {
    return gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/admin-lte/dist/css/AdminLTE.min.css'])
        .pipe(concat('vendor.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gzip())
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('js_vendor', () => {
    return gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js'])
        .pipe(concat('vendor.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gzip())
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('admin_css_vendor', () => {
    return gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/Ionicons/css/ionicons.min.css',
        'bower_components/admin-lte/dist/css/AdminLTE.min.css',
        'bower_components/admin-lte/dist/css/skins/skin-blue.min.css'])
        .pipe(concat('admin_vendor.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gzip())
        .pipe(gulp.dest('assets/css/'))
})

gulp.task('account_css_vendor', () => {
    return gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/Ionicons/css/ionicons.min.css',
        'bower_components/admin-lte/dist/css/AdminLTE.min.css',
        'bower_components/admin-lte/dist/css/skins/skin-blue.min.css'])
        .pipe(concat('account_vendor.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gzip())
        .pipe(gulp.dest('assets/css/'))
})

gulp.task('admin_js_vendor', () => {
    return gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/fastclick/lib/fastclick.js',
        'bower_components/admin-lte/dist/js/adminlte.min.js'])
        .pipe(concat('admin_vendor.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gzip())
        .pipe(gulp.dest('assets/js/'))
})

gulp.task('account_js_vendor', () => {
    return gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/fastclick/lib/fastclick.js',
        'bower_components/admin-lte/dist/js/adminlte.min.js'])
        .pipe(concat('account_vendor.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gzip())
        .pipe(gulp.dest('assets/js/'))
})

gulp.task('default', [
    'sass',
    'coffee',
    'css_vendor',
    'js_vendor',
    'admin_js_vendor',
    'admin_css_vendor',
    'account_js_vendor',
    'account_css_vendor'
]);
gulp.task('watch', [
    'watch_sass',
    'watch_coffee'
]);