module.exports.seed = {
  "users": [
    {
      'id': '583fb8a21e3dbbef46331091',
      'role': 1,
      'username': 'default1',
      'email': 'default1@gmail.com',
      'password': '123456'
    },
    {
      'id': '583fb8a21e3dbbef46331092',
      'role': 1,
      'username': 'default2',
      'email': 'default2@gmail.com',
      'password': '123456'
    },
    {
      'id': '583fb8a21e3dbbef46331093',
      'role': 2,
      'username': 'admin',
      'email': 'admin@admin.com',
      'password': '123456'
    }
  ]
}