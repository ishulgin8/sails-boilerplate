/**
 * AdminController
 *
 * @description :: Server-side logic for managing admin panel
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const path = require('path')
const fs = require('fs')

module.exports = {
  index: (req, res) => {
    res.writeHead(200, {'Content-Type': 'text/html', 'Cache-Control': 'private, no-cache, no-store, must-revalidate', Expires: '-1', Pragma: 'no-cache'})
    fs.createReadStream(path.resolve(__dirname, '../../assets/builds/admin', 'index_prod.html'))
      .pipe(res)
	}
}