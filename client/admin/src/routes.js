import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';

import DashboardComponent from './components/DashboardComponent';

export default <Route path='/admin' component={App}>
  <IndexRoute component={DashboardComponent} />
</Route>
