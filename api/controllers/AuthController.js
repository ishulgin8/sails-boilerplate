/**
 * AuthController
 *
 * @description :: Server-side logic for managing auth
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const passport = require('passport');

module.exports = {
	signin: (req, res) => {
		const errorsExist = !_.isEmpty(req.session.flash) && !_.isEmpty(req.session.flash.signinFormErrors);
    const errors = errorsExist ? _.clone(req.session.flash.signinFormErrors) : false;
    req.session.flash = {};
		return res.view('pages/signin', {
			action: 'signin',
			user: req.user ? req.user : {},
			errors
		})
	},
  signup: (req, res) => {
		const errorsExist = !_.isEmpty(req.session.flash) && !_.isEmpty(req.session.flash.signupFormErrors);
    const errors = errorsExist ? _.clone(req.session.flash.signupFormErrors) : false;
    req.session.flash = {};
		return res.view('pages/signup', {
			action: 'signup',
			user: req.user ? req.user : {},
			errors
		})
	},
	login: (req, res) => {
    passport.authenticate('local', (err, user, info) => {
      if ((err) || (!user)) {
				if (typeof (req.session['flash'] === 'undefined')) {
					req.session['flash'] = {
						signinFormErrors: [info.message]
					};
				} else {
					req.session['flash']['signinFormErrors'] = [info.message];
				}
				return res.redirect('back');
      } else {
				req.logIn(user, err => {
					if (err) {
						if (typeof (req.session['flash'] === 'undefined')) {
							req.session['flash'] = {
								signinFormErrors: [info.message]
							};
						} else {
							req.session['flash']['signinFormErrors'] = [info.message];
						}
						return res.redirect('back');
					} else {
						if (user.role === 2) {
							return res.redirect('/admin');
						} else {
							return res.redirect('/account');
						}
					}
				});
			}
    })(req, res);
  },
	logout: (req, res) => {
    req.logout();
    res.redirect('/');
  }
};
