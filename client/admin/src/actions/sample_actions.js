import { axiosInstance } from '../configs/axios';
import { browserHistory } from 'react-router';
import { defaults } from '../configs/defaults';

const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';