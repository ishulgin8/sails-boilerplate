module.exports = {
  build_population_lookups: (target, populations) => {
    const lookups = populations.map(p => {
      const config_options = sails.config.populations[target] ? sails.config.populations[target] : [];
      let index = config_options.findIndex(x => x.slug === p);
      if (index > -1) {
        return sails.config.populations[target][index].lookup;
      }
    });
    return lookups;
  },
  generate_unique_id: () => {
    let id = '';
    const length = 8;
    const timestamp = +new Date;
    const _getRandomInt = (min, max) => {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    const ts = timestamp.toString();
    const parts = ts.split('').reverse();
    for (let i = 0; i < length; ++i) {
      const index = _getRandomInt(0, parts.length - 1);
      id += parts[index];
    }
    return id;
  },
  isObjectEmpty: obj => {
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false
      }
    }
    return true
  }
}