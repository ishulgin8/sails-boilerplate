import { createStore, applyMiddleware } from 'redux'
import promise from 'redux-promise-middleware'
import thunk from 'redux-thunk'
import rootReducer from '../reducers'

const configureStore = () => createStore(
  rootReducer,
  applyMiddleware(promise(), thunk)
)

export default configureStore
