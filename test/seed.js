const async = require('async');
const seed_config = require('./config').seed;

const seed = () => {
  return new Promise((resolve, reject) => {
    async.waterfall([
      users = cbAsync => {
        async.each(seed_config['users'], (seed_object, callback) => {
          sails.models['users'].create(seed_object).exec((error, data) => {
            callback(null);
          })
        }, error => {
          cbAsync({});
        });
      }
    ], asyncComplete = data => {
      return resolve({});
    });
  })
}

const clean_seed = () => {
  return new Promise((resolve, reject) => {
    async.waterfall([
      users = cbAsync => {
        const remove_ids = seed_config['users'].concat(seed_config['inject_users']).map(seed_task => {
          return seed_task.id;
        });
        async.each(seed_config['users'], (seed_object, callback) => {
          sails.models['users'].destroy({id: {$in: remove_ids}}).exec(error => {
            callback(null);
          })
        }, error => {
          cbAsync({});
        });
      }
    ], asyncComplete = data => {
      return resolve({});
    });
  })
}

module.exports = {
  seed,
  clean_seed
};