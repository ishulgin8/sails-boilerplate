/**
 * isUser
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */

module.exports = (req, res, next) => {
  if (typeof (req.user) !== 'undefined' && req.user.role === 2) {
    return next()
  }
  return res.redirect('/')
}
