const Sails = require('sails');
const async = require('async');
const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';

const seed = () => {
  return new Promise((resolve, reject) => {
    async.waterfall([
      users = cbAsync => {
        async.each(sails.config.seed['users'], (seed_object, callback) => {
          sails.models['users'].create(seed_object).exec((error, data) => {
            callback(null);
          })
        }, error => {
          cbAsync({});
        });
      }
    ], asyncComplete = data => {
      return resolve({});
    });
  })
}

Sails.lift({
  log: {
    level: 'error',
  },
  environment: env,
  hooks: {
    grunt: false,
  }
}, (err, server) => {
  seed().then(result => {
    Sails.lower();
  }).catch(error => {
    throw new Error(error);
  });
});