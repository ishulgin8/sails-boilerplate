import React from 'react'

export default class Footer extends React.Component {
  render() {
    return (
      <footer className='main-footer'>&copy; 2018 BoilerPlate Platform</footer>
    )
  }
}