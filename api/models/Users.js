/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

/**
 * role
 * 1 - user
 * 2 - admin
 * */

const bcrypt = require('bcrypt-nodejs');

module.exports = {
  attributes: {
    role: {
      type: 'integer',
      enum: [1, 2],
      defaultsTo: 1
    },
    username: {
      type: 'string',
      defaultsTo: ''
    },
    fname: {
      type: 'string',
      defaultsTo: ''
    },
    email: {
      type: 'string',
      defaultsTo: ''
    },
    password: {
      type: 'string',
      defaultsTo: ''
    },
    block: {
      type: 'integer',
      enum: [0, 1],
      defaultsTo: 0
    },
    geo: {
      type: 'json',
      defaultsTo: {}
    },
    ip: {
      type: 'string',
      defaultsTo: ''
    },
    is_activated: {
      type: 'integer',
      enum: [0, 1],
      defaultsTo: 0
    }
  },
  customToJSON: function () {
     return _.omit(this, ['password']);
  },
  beforeCreate: function (user, cb) {
    bcrypt.genSalt(10, function (err, salt) {
      bcrypt.hash(user.password, salt, null, function (err, hash) {
        if (err) {
          return cb(err);
        }
        user.password = hash;
        return cb();
      });
    });
  }
};
