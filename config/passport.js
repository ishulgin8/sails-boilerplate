const passport = require('passport'),
      LocalStrategy = require('passport-local').Strategy,
      bcrypt = require('bcrypt-nodejs');

passport.serializeUser((user, cb) => {
  cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
  Users.findOne({id}, (err, user) => {
    cb(err, user);
  });
});

passport.use(new LocalStrategy({
  usernameField: 'username',
  passportField: 'password'
}, (username, password, cb) => {
  Users.findOne({username: username}, (err, user) => {
    if(err) {
      return cb(err);
    }
    if(!user) {
      return cb(null, false, {message: 'Username not found'});
    }
    bcrypt.compare(password, user.password, (err, res) => {
      if(!res) {
        return cb(null, false, { message: 'Invalid Password' });
      }
      let userDetails = {
        email: user.email,
        username: user.username,
        role: user.role,
        id: user.id
      };
      return cb(null, userDetails, { message: 'Login Successful'});
    });
  });
}));