'use strict'

const seed_config = require('../../config').seed;
const ObjectID = require('mongodb').ObjectID;
const expect = require('chai').expect;

describe('Model Service (On Users Collection Example)', () => {
  it('native get should work', () => {
    return ModelService.native_get('users', {}).then(result => {
      const result_data = result.data ? result.data : [];
      expect(result_data).to.have.lengthOf(3);
    });
  });
  it('native create should work', () => {
    const resource = seed_config['inject_users'] && seed_config['inject_users'].length > 0 ? seed_config['inject_users'][0] : {};
    return ModelService.native_create('users', { resource }).then(result => {
      const result_data = result.data ? result.data : 0;
      expect(result_data).to.equal(1);
    });
  });
  it('native update should work', () => {
    const update_id = seed_config['users'] && seed_config['users'].length > 0 ? new ObjectID(seed_config['users'][0]['id']) : '';
    return ModelService.native_update('users', {
      query: {
        _id: update_id
      },
      resource: {
        fname: 'changed'
      }
    }).then(result => {
      const result_data = result.data ? result.data : 0;
      expect(result_data).to.equal(1);
    });
  });
  it('native remove should work', () => {
    const remove_id = seed_config['users'] && seed_config['users'].length > 0 ? new ObjectID(seed_config['users'][0]['id']) : '';
    return ModelService.native_remove('users', {
      query: {
        _id: remove_id
      }
    }).then(result => {
      expect(result).to.equal(true);
    });
  });
});