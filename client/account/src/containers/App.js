import React from 'react'
import PropTypes from 'prop-types'

import Header from '../components/views/partials/Header'
import Footer from '../components/views/partials/Footer'
import Navigation from '../components/views/partials/Navigation'

export default class App extends React.Component {
  static propTypes = {
    children: PropTypes.node
  }

  render() {
    const { children, products } = this.props
    return (
      <div className='wrapper'>
        <Header />
        <Navigation />
        <div className='content-wrapper'>
          {children}
        </div>
        <Footer />
        <div className='control-sidebar-bg'></div>
      </div>
    )
  }
}