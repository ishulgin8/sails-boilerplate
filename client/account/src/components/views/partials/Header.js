import React from 'react'
import { Link } from 'react-router'

export default class Header extends React.Component {
  render() {
    return (
      <header className='main-header'>
        <a href='#' className='logo'>
          <span className='logo-mini'><b>A</b></span>
          <span className='logo-lg'>Account</span>
        </a>
        <nav className='navbar navbar-static-top'>
          <a href='#' className='sidebar-toggle' data-toggle='push-menu' role='button'>
            <span className='sr-only'>Toggle navigation</span>
          </a>
          <div className='navbar-custom-menu'>
            <ul className='nav navbar-nav'>
              <li className='dropdown user user-menu'>
                <a href='#' className='dropdown-toggle' data-toggle='dropdown'>
                  {/*<img src='' className='user-image' />*/}
                  <span className='hidden-xs'>User</span>
                </a>
                <ul className='dropdown-menu'>
                  <li className='user-footer'>
                    <a className='btn btn-default btn-flat' href='/logout'>Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    )
  }
}