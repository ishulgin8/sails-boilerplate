module.exports = {
  native_create: (model, options) => {
    return new Promise((resolve, reject) => {
      sails.models[model].native((error, collection) => {
        if (error) {
          return reject(error);
        }
        let resource = options.resource ? options.resource : {};
        resource['createdAt'] = new Date();
        resource['updatedAt'] = new Date();
        collection.insertMany([resource], (error, result) => {
          if (error) {
            let error_response = sails.config.custom.get_error_template();
            error_response.msg = error;
            return reject(error_response);
          }
          let response_data = sails.config.custom.get_success_template();
          response_data.data = result.result.n ? result.result.n : 0;
          return resolve(response_data);
        })
      })
    })
  },
  native_update: (model, options) => {
    return new Promise((resolve, reject) => {
      sails.models[model].native((error, collection) => {
        if (error) {
          return reject(error);
        }
        const query = options.query ? options.query : {};
        let resource = options.resource ? options.resource : {};
        resource['updatedAt'] = new Date();
        collection.updateOne(query, {$set: resource}, (error, result) => {
          if (error) {
            let error_response = sails.config.custom.get_error_template();
            error_response.msg = error;
            return reject(error_response);
          }
          let response_data = sails.config.custom.get_success_template();
          response_data.data = result.result.n ? result.result.n : 0;
          return resolve(response_data);
        })
      })
    })
  },
  native_remove: (model, options) => {
    return new Promise((resolve, reject) => {
      sails.models[model].native((error, collection) => {
        if (error) {
          return reject(error);
        }
        collection.deleteOne(options.query ? options.query : {}, (error, result) => {
          if (error) {
            return reject(error);
          }
          return resolve(result.deletedCount && result.deletedCount > 0 ? true : false);
        })
      })
    })
  },
  native_get: (model, options) => {
    let query = options.query ? options.query : {};
    const lookups = options.lookups ? options.lookups: [];
    const limit = options.limit ? options.limit : 0;
    let query_aggregation = [
      {
        $match: query
      }
    ].concat(lookups);
    if (limit !== 0) {
      query_aggregation.push({
        $limit: limit
      })
    }
    if (options.skip) {
      query_aggregation.push({
        $skip: options.skip
      })
    }
    if (options.sort) {
      let sort_object = {
        $sort: {}
      };
      for (let i in options.sort) {
        sort_object['$sort'][i] = options.sort[i];
      };
      query_aggregation.push(sort_object);
    }
    return new Promise((resolve, reject) => {
      sails.models[model].native((error, collection) => {
        if (error) {
          return reject(error);
        }
        collection.aggregate(query_aggregation, (error, result) => {
          if (error) {
            let error_response = sails.config.custom.get_error_template();
            error_response.msg = error;
            return reject(error_response);
          }
          let response_data = sails.config.custom.get_success_template();
          response_data.data = result;
          return resolve(response_data);
        })
      });
    });
  }
}