import React from 'react'

export class Table extends React.Component {
  componentDidMount() {
      $(this.refs.main).DataTable({
          dom: '<"data-table-wrapper"t>',
          data: this.props.opts.data,
          columns: this.props.opts.columns,
          ordering: false
      });
  }
  componentWillUnmount() {
      $('.data-table-wrapper')
      .find('table')
      .DataTable()
      .destroy(true);
  }
  shouldComponentUpdate() {
      return false;
  }
  render() {
    return (
      <div>
        <table ref='main' />
      </div>
    );
  }
}