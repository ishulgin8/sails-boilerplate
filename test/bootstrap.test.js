const Sails = require('sails');
const unit_tests_seed = require('./seed');

before(done => {
  Sails.lift({
    log: {
      level: 'error',
    },
    environment: 'tdd',
    models: {
      migrate: 'drop',
    },
    hooks: {
      grunt: false,
    },
  }, (err, server) => {
    if (err) {
      return done(err);
    }
    unit_tests_seed.seed().then(result => {
      done(null, sails);
    }).catch(error => {
      throw new Error(error);
    });
  });
});

after(done => {
  unit_tests_seed.clean_seed().then(result => {
    Sails.lower(done);
  }).catch(error => {
    throw new Error(error);
  });
});
